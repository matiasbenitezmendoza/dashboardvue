export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer'
    },
    {
      name: 'Registro',
      url: '/registro',
      icon: 'icon-people'
    },
    {
      name: 'Red',
      url: '/red',
      icon: 'icon-people'
    }
  ]
}

