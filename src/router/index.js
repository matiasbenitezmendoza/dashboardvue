import Vue from 'vue'
import Router from 'vue-router'
import { Global } from "../data/Global";
import axios from "axios";

// Containers
const DefaultContainer = () => import('@/containers/DefaultContainer')


// Views
const Dashboard = () => import('@/views/Dashboard')
const Login = () => import('@/views/Login')
const Registro = () => import('@/views/Registro')
const Redes = () => import('@/views/Redes')
const Roles = () => import('@/views/Roles')
const AgregarRoles = () => import('@/views/AgregarRoles')
const EditarRoles = () => import('@/views/EditarRoles')
const Usuarios = () => import('@/views/Usuarios')
const EditarUsuarios = () => import('@/views/EditarUsuarios')
const PerfilUsuarios = () => import('@/views/PerfilUsuarios')
const Perfil = () => import('@/views/Perfil')
const Geoanalisis = () => import('@/views/Geoanalisis')
const Analisis = () => import('@/views/Analisis')


Vue.use(Router)



const router = new Router({
  mode: 'history', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Home',
      component: DefaultContainer,
      children: [
        {
          path: 'dashboard',
          name: 'dashboard',
          component: Dashboard
        },
        {
          path: 'registro',
          name: 'registro',
          component: Registro
        },
        {
          path: 'usuarios',
          name: 'usuarios',
          component: Usuarios
        },
        {
          path: 'redes',
          name: 'redes',
          component: Redes
        },
        {
          path: 'roles',
          name: 'roles',
          component: Roles
        },
        {
          path: 'agregar_rol',
          name: 'agregar_rol',
          component: AgregarRoles
        },
        {
          path: 'editar_rol/:id',
          name: 'editar_rol',
          component: EditarRoles
        },
        {
          path: 'editar_usuario/:id',
          name: 'editar_usuario',
          component: EditarUsuarios
        },
        {
          path: 'perfil_usuario/:id',
          name: 'perfil_usuario',
          component: PerfilUsuarios
        },
        {
          path: 'perfil',
          name: 'perfil',
          component: Perfil
        },
        {
          path: 'geoanalisis',
          name: 'geoanalisis',
          component: Geoanalisis
        },
        {
          path: 'analisis',
          name: 'analisis',
          component: Analisis
        }
      ],
      meta : {
        autentificado : true
      }
    },
    {
      path: '/login',
      redirect: '/login',
      name: 'DefaulLogin',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: '',
          name: 'Login',
          component: Login
        }
      ]
    },
    {
      path: '*',
      redirect: '/login'
    }

  ]

})


router.beforeEach((to, from, next) => {

      let autorizacion = to.matched.some(record => record.meta.autentificado);
      let authUser = false;
      let permiso = false;


      if (router.app.$session.exists()) {
           authUser =  router.app.$session.get('dashboard_sesison');
           var cad_permiso = router.app.$session.get("dashboard_permisos");
           var permisos = JSON.parse(cad_permiso);
           console.log(permisos);

           if(permisos[to.name] != undefined){
               permiso = permisos[to.name];
           }
      }
      console.log(to.name);

      console.log(permiso);

      if (autorizacion && authUser !== "sucess") {
          next("login");
      }
      else if (autorizacion && authUser === "sucess" && permiso === false) {
         next("dashboard");
      }
      else if(to.name === "Login" && authUser === "sucess" ){
          next("dashboard");
      }
      else{
           console.log("else");
           console.log(autorizacion);
           console.log(authUser);

            next();
      }

})


export default router;


/*
import Vue from 'vue'
import Router from 'vue-router'
import { Global } from "../data/Global";
import axios from "axios";

// Containers
const DefaultContainer = () => import('@/containers/DefaultContainer')


// Views
const Dashboard = () => import('@/views/Dashboard')
const Login = () => import('@/views/Login')
const Registro = () => import('@/views/Registro')
const Red = () => import('@/views/Red')
const Roles = () => import('@/views/Roles')
Vue.use(Router)



const router = new Router({
  mode: 'history', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'home',
      component: DefaultContainer,
      children: [
        {
          path: 'dashboard',
          name: 'dashboard',
          component: Dashboard
        }
      ],
      meta : {
        autentificado : true
      }
    },
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Usuario',
      component: DefaultContainer,
      children: [
        {
          path: 'registro',
          name: 'usuarios',
          component: Registro
        }
      ],
      meta : {
        autentificado : true
      }
    },
    {
      path: '/',
      redirect: '/dashboard',
      name: 'DefaulRed',
      component: DefaultContainer,
      children: [
        {
          path: 'red',
          name: 'red',
          component: Red
        }
      ],
      meta : {
        autentificado : true
      }
    },
    {
      path: '/',
      redirect: '/dashboard',
      name: 'DefaulRoles',
      component: DefaultContainer,
      children: [
        {
          path: 'roles',
          name: 'roles',
          component: Roles
        }
      ],
      meta : {
        autentificado : true
      }
    },
    {
      path: '/login',
      redirect: '/login',
      name: 'DefaulLogin',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: '',
          name: 'Login',
          component: Login
        }
      ]
    },
    {
      path: '*',
      redirect: '/login'
    }

  ]

})


router.beforeEach((to, from, next) => {

      let autorizacion = to.matched.some(record => record.meta.autentificado);
      let authUser = false;
      let permiso = false;


      if (router.app.$session.exists()) {
           authUser =  router.app.$session.get('dashboard_sesison');
           var cad_permiso = router.app.$session.get("dashboard_permisos");
           var permisos = JSON.parse(cad_permiso);
           if(permisos[to.name] != undefined){
               permiso = permisos[to.name];
           }
      }
      console.log(permiso);

      if (autorizacion && authUser !== "sucess") {
          next("login");
      }
      else if (autorizacion && authUser === "sucess" && permiso === false) {
         next("dashboard");
      }
      else if(to.name === "Login" && authUser === "sucess" ){
          next("dashboard");
      }
      else{
           console.log("else");
           console.log(autorizacion);
           console.log(authUser);

            next();
      }

})


export default router;
 */