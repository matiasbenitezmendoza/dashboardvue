// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import './polyfill'
// import cssVars from 'css-vars-ponyfill'
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import router from './router/index'
import Vuelidate from 'vuelidate'
import VueSession from 'vue-session'

 

import VueNotifications from 'vue-notifications'
import miniToastr from 'mini-toastr'// https://github.com/se-panfilov/mini-toastr
const toastTypes = {
  success: 'success',
  error: 'error',
  info: 'info',
  warn: 'warn'
}
miniToastr.init({types: toastTypes})
miniToastr.setIcon('success', 'img', {src: '/img/logos/logo_alerta.png', style: 'vertical-align: center; witdh: 20px;'})
miniToastr.setIcon('error', 'img', {src: '/img/logos/logo_alerta.png', style: 'vertical-align: center; witdh: 20px;'})

function toast ({title, message, type, timeout, cb}) {
  return miniToastr[type](message, title, timeout, cb)
}

const options_toast = {
  success: toast,
  error: toast,
  info: toast,
  warn: toast
}

Vue.use(VueNotifications, options_toast)

// todo
// cssVars()
var options = {
  persist: true
}

Vue.use(VueSession, options)
Vue.use(BootstrapVue)
Vue.use(Vuelidate)

//import App from './App'
//import AppDark from './AppDark'






if (Vue.prototype.$session.exists()) {
    var type = Vue.prototype.$session.get('dashboard_tema');
    if(type != "white"){
                    var App = () => import('@/AppDark');

                    new Vue({
                      el: '#app',
                      router,
                      template: '<App/>',
                      components: {
                        App
                      }
                    })
    }else{
                    var App = () => import('@/App');
                    new Vue({
                      el: '#app',
                      router,
                      template: '<App/>',
                      components: {
                        App
                      }
                    })
    } 
}else{
  var App = () => import('@/AppDark');
  new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: {
      App
    }
  })
}



 

/* eslint-disable no-new */
